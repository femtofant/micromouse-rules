---
title: RSA MicroMouse Rules
version: 2024-01-12 Revision 3
---
# 1. Introduction

These rules are based on the MicroMouse Competition Rules for the IEEE R2 SAC 2020 (link).
They are modified to fit this competition.

# 2. Participant Eligibility

## 2.1. RSA Membership

To participate, all team members shall either join the RSA at least one week before the competition (4th of May) or pay a 10€ fee per team member.

## 2.2. Team Composition

Teams shall consist of one to three participants.

## 2.3. Funding

**Teams may not spend more than 150€ to develop the robots,** including any components and fabrication costs, and excluding shipping. When ordering from TinyTronics or OpenCircuits through the RSA, teams might get a discount, in which case the discounted price will be taken into account in the team budget.
Teams may make a common PCB or TinyTronics order through the RSA, in which case the shipping will be paid by the RSA.

# 3. Robot

## 3.1. Fabrication

The MicroMouse robot submitted by a team must be designed and built from scratch.

## 3.2. Self Containment

The MicroMouse robot shall be self-contained (no remote controls).
The robot shall not use an energy source employing a combustion process.

## 3.3. Dislodged Parts

The MicroMouse robot shall not separate from any part of itself whilst navigating the maze.
To complete the maze, the robot in its entirety must enter the center of the maze.

## 3.4. Method of Movement

The MicroMouse robot shall not jump over, fly over, climb, scratch, cut, burn, mark, damage, or destroy the walls of the maze.

## 3.5. Size

The MicroMouse robot shall not be larger either in length or in width than 15 centimeters.
The dimensions of a MicroMouse robot that changes its geometry shall not be greater than 15cm x 15cm (length and width, respectively).
There are no restrictions on the height of the robot.

## 3.6. Inspection

All MicroMouse robots are subject to inspection prior to starting their competition, to ensure they are within the specifications outlined by these rules, and that they do not pose potential safety hazards.

## 3.7. Rules Violation

Any violation of these rules will constitute immediate disqualification from the contest and ineligibility for any associated prizes.

# 4. Maze

## 4.1. Maze Dimensions

The maze is composed of 12cm x 12cm unit squares arranged to form a 16 x 16 unit grid.
The walls of the units of the maze are 5cm high and roughly 3.5mm thick (assume 5% tolerance for mazes).
An outside wall encloses the entire maze.

## 4.2. Maze Colouration

The sides of the maze walls are white, the tops of the walls are red, and the floor is black.
The maze is made of wood, finished with non-gloss paint.

## 4.3. Fabrication Inconsistencies

Do not assume the walls are consistently white, or that the tops of the walls are consistently red, or that the floor is consistently black.
Fading may occur and parts from different mazes may be used.
Do not assume the floor provides a given amount of friction.
It is simply painted plywood and may be quite slick.
The maze floor may be constructed using multiple sheets of plywood.
Therefore there may be a seam between the two sheets on which any low-hanging parts of the robot may snag.

## 4.4. Start/End Zones

The starting square is located on the edge of the maze.
The starting square is bounded on three sides by walls.
The start line is located between the first and second squares.
As the mouse exits the starting square (signified by crossing the start line), the run timer starts.
The destination is a gateway to the four unit square at the center of the maze.
The destination square has only one gateway.

## 4.5. Lattice Points

Small square posts, each 4mm x 4mm, at the four corners of each unit square are called lattice points.
The maze is assembled so that there is at least one wall at each lattice point.

## 4.6. Multiple Paths

Multiple paths from the starting square to the destination square are allowed and should be expected.
The destination square on the competition field (but not on the practice field) will be positioned so that a wall-hugging mouse will NOT be able to find it.

# 5. Competition

## 5.1. Time

Each team is allocated 10 minutes of access to the maze, starting when the competition administrator acknowledges the team and grants access to the maze.
Any time used to adjust the team’s MicroMouse robot between runs is included in these 10 minutes.
A runtime is recorded for each run (from the start cell to the center zone) in which the robot successfully reaches the destination square.
The minimum run time within the 10-minute trial shall be the mouse’s official time.

## 5.2. Stopping/Removing the MicroMouse Robot

Each run shall be made from the starting square.
Multiple runs, or run attempts, may be made within the allotted 10 minute maze time.
The team may abort a run at any time, and return the MicroMouse robot to the starting square.
If the robot has reached the destination square and has acquired a “run time,” the robot may take the maze back to the corner starting square on its own.
Alternatively, it may be removed at any time without affecting the runtime of that run.

## 5.3. Reprogramming After Reveal

After the competition maze is revealed to the teams at the start of the competition, the operator shall not reprogram his or her MicroMouse robot, but may elect to change the positions of switches located on the robot.

## 5.4. Room Contitions

The illumination, temperature, and humidity of the room shall be those of an ambient environment.
(5°C to 45°C, 0% to 95% humidity, non-condensing).
Do not make any assumptions about the amount of sunlight, incident light, or fluorescent light that may be present at the competition site.

## 5.5. Run Time

The run timer will start when the front edge of the Micromouse robot crosses the start line and stops when the front edge of the mouse crosses the finish line.
The start line is at the boundary between the starting unit square and the next unit square clockwise.
The finish line is at the entrance to the destination square.

## 5.6. Starting a Run

Every time the MicroMouse robot leaves the starting square, a new run begins.
If the robot does not enter the destination square, no runtime is recorded.
For example, if the robot re-enters the starting square (before entering the destination square) on a run, that run is aborted, and a new runtime will begin when the robot leaves the starting square.

## 5.7. Continued Navigation

If the MicroMouse robot continues to navigate the maze after reaching the destination square, the time taken will not count toward any run.
The robot may and should make several runs without being touched by the operator.
Once the robot has found the center, it is common practice to explore the maze via an alternate path on the return trip to the starting square.

## 5.8. Modifying the Robot

The team may not feed information about the maze to the MicroMouse robot.
Therefore, changing ROMs or downloading programs is NOT allowed once the maze is revealed.
However, participants are allowed to do the following:

- Change switch settings (e.g. to select algorithms)
- Replace batteries between runs
- Adjust sensors
- Change speed settings
- Make repairs

## 5.9. Changing the Robot’s Weight

After the team’s 10 minute time allotment begins, they shall not alter their MicroMouse robot in a manner that alters its weight (e.g. removal of a bulky sensor array or switching to lighter batteries to get better speed after mapping the maze is not allowed).
The judges shall arbitrate all interactions with the robot.

# 6. Scoring

## 6.1. Scoring

First place goes to the team with the shortest official time (without the team touching its MicroMouse robot during runs).
Second prize to the team with the next shortest time, and so on.
Teams with the MicroMouse robot that does not enter the center square will be ranked by the judges based on the following criteria:

- How close the robot gets to the destination square without being touched
- Evidence that the mouse knows where it is relative to the destination square

If, on occasion, the robot becomes immobilized in a corner or on a wall, the team may manually intervene to correct the problem (with care not to modify the mouse’s intended direction of movement.
The frequency of such corrections will be considered by the judges while scoring.

## 6.2. Requesting Breaks

If requested, a break may be provided to the team after the completion of a run if another team is waiting to compete.
The 10 minute timer will stop.
When the team resumes their play, the 10 minute timer will continue.
Judges and administrators shall determine whether a breaks are allowed on a case by case basis.

## 6.3. Judges’ Discretion

The judges reserve the right to ask the team for an explanation of their MicroMouse robot and its actions.
The judges also reserve the right to stop a run, declare disqualification, or give instructions as appropriate (e.g., if the structure of the maze is jeopardized by continuing operation of the mouse).
